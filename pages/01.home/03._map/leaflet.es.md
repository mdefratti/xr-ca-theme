---
title: Chapter Map
cache_enable: false
chapters:
    south-bay:
        id: south-bay
        name: XR South Bay
        logo: xrsb-logo.png
        lat: 37.3393857
        lng: -121.8949555 
        twitter: https://twitter.com/XRSouthBay
        instagram: https://www.instagram.com/xr_south_bay/
        facebook: https://www.facebook.com/pages/category/Community/Extinction-Rebellion-South-Bay-404963127035060/
        website: 
        signup: https://actionnetwork.org/forms/xr-south-bay-sign-up-form
        introduction: This is the South SF Bay Area chapter of the Extinction Rebellion.  Covering Silicon Valley and surrounding areas.  Meets regularly in San Jose, California.  Founded on love, and community based for those wanting to work in coalition against the sixth mass Extinction.
        donate: paypal.me/xrsouthbay
    
    los-angeles:
        id: los-angeles
        name: XR Los Angeles
        logo: xrla-logo.png
        lat: 34.0522342
        lng: -118.2436849
        twitter: https://twitter.com/XRebellionLA
        instagram: https://www.instagram.com/xrebellionla/
        facebook: https://www.facebook.com/ExtinctionRebellionLA
        website: https://xrla.org
        signup:
        introduction: This is the Los Angeles chapter of Extinction Rebellion. 
        donate: 

---