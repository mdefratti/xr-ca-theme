---
title: 'Homepage Demands'
hide_git_sync_repo_url: false
menu: Highlights
class: small
demands:
    -
        header: 'Tell The Truth'
        icon: 'fas fa-bullhorn'
        class: pink
        text: 'Our government must tell the truth about how deadly our climate situation truly is and it must reverse all policies not in alignment with the terrible realities of our present life-threatening climate and ecological realities.'
    -
        header: 'Net Zero By 2025'
        icon: 'fas fa-leaf'
        class: yellow
        text: 'Our government must quickly enact legally binding policies to reduce carbon emissions to net zero by 2025.'
    -
        header: "Citizens' Assembly"
        icon: 'fas fa-fist-raised'
        class: blue
        text: "Citizens' assemblies must be created and empowered to ensure these policies are enacted by government -- and work!"
    -
        header: 'Just Transition'
        icon: 'fas fa-balance-scale'
        class: purple
        text: "A just transition to sustainable governance which acknowledges and repairs historical and structural environmental injustice, as well as legal rights for all ecosystems, are non-negotiable parts of XRCAL's requirements."
---

# Demands{.black-line}
