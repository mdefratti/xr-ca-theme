---
title: Local Development Using Docker
taxonomy:
    category: docs
---

## Prerequisites 

### Insall and start Docker on your computer
Below are external instructions to install docker and docker-compose for your computer.  

[Install Docker](https://docs.docker.com/install/)  
[Install Docker-compose](https://docs.docker.com/compose/install/)  


## Launching site  
The first step in launching the site for local development is to create a directory on your computer that will hold the site. 

### Instructions for L[iU]nix computers (Mac & Linux)  

#### Create and enter site directory
`mkdir -p ~/repose/xrla-site && cd ~/repose/xrla-site`  

#### Create docker-compose configuration
`touch docker-compose.yaml`  

#### Copy repo into the current directory and name /user
`git clone git@gitlab.com:xrla/xrla-user-directory.git user`  

#### Checkout develop
`git checkout develop`  

#### Get your current users UID
`ls -ln`  
Example results: `drwxr-xr-x@ 16 501  20   512 Jul 15 17:30 user`  
In the above results `501` is the numerical UID that you will want to use in your Docker Compose environment variable `PUID`.  

#### Docker compose configuration
```yaml
version: '3'
services:
  site:
    image: marvinroman/grav-docker:release-0.2 
    ports:
      - "80:80" # Open port 80 on the docker network and forward to port 80 inside the docker
    environment:
      - "ERRORS=1" # Display errors
      - "DOMAIN=localhost" # Set domain
      - "MAX_EXECUTION_TIME=120" # Increase max execution time
      - "PHP_MEM_LIMIT=256" # Increase PHP memory limit
      - "PUID=501" # Change to UID of your current computer user
      - "FASTCGI_CACHE=0" # Turn off NGINX full page caching
      - "NGINX_DEBUG_HEADERS=1" # Turn on NGINX debug headers for caching
      - "TIMEZONE=America/Los_Angeles" # Set server & PHP timezone
      - "MULTISITE=subdirectory" # enable multisite capabilities via a subdirectory. Allows you to run a documentation site like /site-docs in this one
      - "ENABLE_SASS=1" # installs sass to make it available for scss compiling
    volumes:
      - "./user:/var/www/html/user:cached" # mount your local user directory into docker so you can see development changes in real time
      - "./backup:/var/www/html/backup:cached" # mount a local directory for backups to be saved to your computer
```  

#### Start the docker 
Startup may take a couple of minutes since it does some setup as it starts up.  
`docker-compose up -d`  
To confirm that your container is running: `docker ps` 

#### Visit the site
[localhost](http://localhost)  

## Start development
Open up the `/user` folder that you cloned in your favorite IDE.  

### CSS Compiling
If are you making CSS alterations you will need to alter the SCSS files within `/user/themes/xrla/scss`.  

To be able to compile the first step is to get your containers name via the following command: `docker ps`.  

Example result: 
```
CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS                                                NAMES
0aeb2d6b922d        marvinroman/grav-docker:develop   "docker-php-entrypoi…"   30 minutes ago      Up 30 minutes       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 9000/tcp   xrla-grav_site_1
```

In the above result the docker name to copy for the following commands would be `xrla-grav_site_1`.  

You can compile the css manually after changes via the following command: `docker exec xrla-grav_site_1 sass --style=compressed user/themes/xrla/scss/:user/themes/xrla/css-compiled/`  

You can compile on the fly after every change is made with the `--watch` flag with the following command: `docker exec xrla-grav_site_1 sass --watch --style=compressed user/themes/xrla/scss/:user/themes/xrla/css-compiled/`  