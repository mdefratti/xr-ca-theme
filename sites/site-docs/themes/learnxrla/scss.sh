#!/bin/sh
WEBROOT=/var/www/html/user/sites/site-docs/themes/learnxrla
sass --watch ${WEBROOT}/scss/theme.scss ${WEBROOT}/css-compiled/theme.css &
sass --watch ${WEBROOT}/scss/nucleus.scss ${WEBROOT}/css-compiled/nucleus.css &